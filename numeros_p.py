#Programa que define si un número es primo o no
#autor: Danilo Delgado
#email: edwin.delgado@unl.edu.ec

num = int(input("Ingrese un número: "))

def numero_primo(num):
    valor = range(2, num)
    c = 0
    for n in valor:
        if num % n == 0:
            c = c+1

    if c > 0 or num < 2:
        print("El número ingresado no es primo")
    else:
        print("El nÚmero ingresado es primo")

numero_primo (num)

